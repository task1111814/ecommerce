<?php

// use App\Http\Controllers\Auth\LoginController;

use App\Http\Controllers\CartController;
use App\Http\Controllers\ForgotPasswordController;
// use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogOutController;
use App\Http\Controllers\RegisteredUserController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

///auth//////////////
Route::post('/login', [LoginController::class,'login'])->middleware(['guest:sanctum']);
Route::post('/register', [RegisteredUserController::class, 'store']);
Route::post('/logout',[LogOutController::class,'logout'])->middleware('auth:sanctum');
Route::post('password/email',  ForgotPasswordController::class);
Route::post('password/reset', ResetPasswordController::class);
///////////////////////////////////////////////
Route::post('/addCart/{id}',[CartController::class,'store'])->middleware(['auth:sanctum'])->name('add_cart'); //add to cart
Route::get('/Cart',[CartController::class,'show'])->middleware(['auth:sanctum'])->name('show_cart'); //show items in cart
Route::put('/Cart/{id}',[CartController::class,'update'])->middleware(['auth:sanctum'])->name('edit_cart'); //update items in cart
Route::delete('/Cart/{id}',[CartController::class,'destroy'])->middleware(['auth:sanctum'])->name('edit_cart'); //delete items in cart
