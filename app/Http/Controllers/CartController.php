<?php

namespace App\Http\Controllers;

use App\Helpers\ApiResponse;
use App\Models\Cart;
use App\Http\Requests\StoreCartRequest;

use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Exists;

// use Illuminate\Support\Facades\Request as FacadesRequest;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Cart::create([

        // ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,$id)
    {
        $validated = $request->validate([
            'qty'=>'required|integer'
        ]);

         if( $product=Product::find($id)){
             Cart::create([
            'name'=>$product->p_name,
            'img'=>"img.png",
            'price'=>$product->p_price,
            'qty'=>$request->qty,
            'user_id'=>Auth::user()->id,
            'pro_id'=>$product->id

        ]);

        return ApiResponse::sendResponse(201,"added To Cart Successfully",);
         }

         else{
            return ApiResponse::sendResponse(404,"the product is out of stock",);
         }




    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $user=Auth::user()->id;
        $cart=  Cart::where("user_id","=",$user)->get();
       if ($cart) {
         return ApiResponse::sendResponse(201,"shopping cart",$cart);
       }



    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Cart $cart ,Request $request ,$id)
    {

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Cart $cart ,$id)
    {
        $validated = $request->validate([
            'qty'=>'required|integer'
        ]);

            Cart::where("user_id","=",Auth::user()->id)->where("id","=",$id)-> update([
            'qty'=>$request->qty

        ]);

        return ApiResponse::sendResponse(201,"updated successfully");

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Cart $cart ,$id)
    {

           Cart::where("user_id","=",Auth::user()->id)->where("id","=",$id)->delete();
           return ApiResponse::sendResponse(201,"deleted successfully");

    }
}
