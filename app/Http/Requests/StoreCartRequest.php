<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules()
    {

        return [
            'name'=>'required',
            'img'=>'required|image|max:3000',
            'price'=>'required|integer|money_format',
            'qty'=>'required|integer',
            'user_id'=>Auth::user()->id,
            'pro_id'=>"integer",

        ];
    }

    public function messages()
    {
        return[
        'pro_id.exists'=>'product_id doesnot exist'
    ];

    }
}
